<?php
/**
 * @package n3t oEmbed
 * @author Pavel Poles - n3t.cz
 * @copyright (C) 2020-2024 - Pavel Poles - n3t.cz
 * @license GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
**/

/**
 *  Variables:
 *  $article - article object
 *  $url - url of post
**/

defined( '_JEXEC' ) or die( 'Restricted access' );
?>
<div class="oembed oembed-error">
  <a href="<?php echo $url; ?>"><?php echo $url; ?></a>
</div>
