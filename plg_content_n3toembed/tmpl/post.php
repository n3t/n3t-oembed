<?php
/**
 * @package n3t oEmbed
 * @author Pavel Poles - n3t.cz
 * @copyright (C) 2020-2024 - Pavel Poles - n3t.cz
 * @license GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
**/

/**
 *  Variables:
 *  $article - article object
 *  $post - oEmbed post array
 *  $params - plugin params
**/

defined( '_JEXEC' ) or die( 'Restricted access' );
?>
<div class="oembed">
  <?php
    if (isset($post['html_original']))
      echo $post['html_original'];
    elseif (isset($post['html']))
      echo $post['html'];
  ?>
</div>
