<?php
/**
 * @package n3t oEmbed
 * @author Pavel Poles - n3t.cz
 * @copyright (C) 2020-2024 - Pavel Poles - n3t.cz
 * @license GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 **/

defined('_JEXEC') or die;

use Joomla\CMS\Factory;
use Joomla\CMS\HTML\HTMLHelper;
use Joomla\CMS\Log\Log;
use Joomla\CMS\Plugin\CMSPlugin;
use Joomla\CMS\Plugin\PluginHelper;

use Embera\Embera;
use Joomla\CMS\Uri\Uri;
use Joomla\CMS\Version;

class plgContentN3tOEmbed extends CMSPlugin
{

	protected $autoloadLanguage = true;

	public function __construct($subject, $config)
	{
		parent::__construct($subject, $config);

		if (!!$this->params->get('responsive', 0))
			HTMLHelper::stylesheet('plg_content_n3toembed/responsive.min.css', false, true);

		if (!!$this->params->get('log_errors', 0))
			Log::addLogger(
				['text_file' => 'n3toembed.php'],
				Log::ALL,
				['n3toembed']
			);
	}

	private function cacheId($url)
	{
		return md5($url);
	}

	private function preparePost(array &$post): void
	{
		if ($post['embera_provider_name'] == 'Youtube' && $this->params->get('youtube_jsapi')) {
			$post['html'] = preg_replace_callback('~src="([^"]+)"~', function ($matches) {
				$url = new Uri($matches[1]);
				$url->setVar('enablejsapi', '1');
				$url->setVar('origin', Uri::base());
				return 'src="' . $url->toString() . '"';
			}, $post['html']);
		}
	}

	public function renderEmbed($article, $url)
	{
		$params = [];
		$params['https_only'] = !!$this->params->get('https_only', 1);
		$params['fake_responses'] = (int)$this->params->get('fake_responses', '1');
		$params['responsive'] = !!$this->params->get('responsive', 0);

		if ((int)$this->params->get('maxwidth'))
			$params['maxwidth'] = (int)$this->params->get('maxwidth');
		if ((int)$this->params->get('maxheight'))
			$params['maxheight'] = (int)$this->params->get('maxheight');

		if ($this->params->get('fb_app_id') && $this->params->get('fb_app_secret'))
			$params['access_token'] = $this->params->get('fb_app_id') . '|' . $this->params->get('fb_app_secret');

//		if (Version::MAJOR_VERSION == 3)
		// TODO dořešit namespace v library
			jimport('Embera.Embera');
		$embera = new Embera($params);

		$posts = $embera->getUrlData($url);

		if ($embera->hasErrors() && $this->params->get('log_errors', 0)) {
			foreach ($embera->getErrors() as $error)
				Log::add($error, Log::ERROR, 'n3toembed');
		}

		ob_start();

		if ($posts) {
			$post = array_shift($posts);
			$this->preparePost($post);
			include PluginHelper::getLayoutPath($this->_type, $this->_name, 'post');
		} else
			include PluginHelper::getLayoutPath($this->_type, $this->_name, 'error');

		return ob_get_clean();
	}

	public function onContentPrepare($context, &$article, $params, $page = 0)
	{
		// TODO only frontend HTML
		if (stripos($article->text, '{oembed') === false)
			return;

		$cache = Factory::getCache('n3toembed');
		if ($cachetime = $this->params->get('cachetime'))
			$cache->setLifeTime($cachetime);

		preg_match_all('~\{oembed(\s|\x{00a0}|&nbsp;)+(.*?)}~iu', $article->text, $matches, PREG_SET_ORDER);

		if ($matches) {
			foreach ($matches as $match) {
				$url = strip_tags($match[2]);
				$cacheId = $this->cacheId($url);
				static $outputs = [];

				if (!isset($outputs[$cacheId])) {
					$output = $cache->get([$this, 'renderEmbed'], [$article, $url], $cacheId);
					$outputs[$cacheId] = $output;
				}

				$article->text = str_replace($match[0], $outputs[$cacheId], $article->text);
			}
		}
	}

}
