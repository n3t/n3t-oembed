n3t oEmbed
==========

[Embera][EMBERA] implementation for Joomla!

Install instructions
----------------------------
 * install package using Joomla! installer
 * enable plugin

[EMBERA]: https://github.com/mpratt/Embera