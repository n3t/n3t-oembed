Release notes
=============

3.0.x
-----

#### 3.0.6
- Embera v. 2.0.36

#### 3.0.5
- Embera v. 2.0.25

#### 3.0.4
- Embera v. 2.0.22
- corrected typo in plugin code

#### 3.0.3
- Embera v. 2.0.18

#### 3.0.2
- optional responsive stylesheet

#### 3.0.1
- Embera v. 2.0.14
- support for maxwidth and maxheight parameters
- support for responsive, https_only and fake_response parameters

#### 3.0.0
- initial release
