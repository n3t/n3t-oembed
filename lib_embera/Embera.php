<?php
/**
 * @package n3t oEmbed
 * @author Pavel Poles - n3t.cz
 * @copyright (C) 2020-2024 - Pavel Poles - n3t.cz
 * @license GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 **/

defined('JPATH_PLATFORM') or die;

use Joomla\CMS\Version;

if (Version::MAJOR_VERSION >= 4)
	JLoader::registerNamespace('Embera', __DIR__ . '/src/Embera/');
else
	JLoader::registerNamespace('Embera', __DIR__ . '/src/');

